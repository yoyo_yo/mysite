from __future__ import unicode_literals

from django.db import models

# Create your models here.
class abtest_schema(models.Model):
    name = models.CharField(max_length = 100, primary_key = True)
    abType = models.IntegerField()
    choice = models.IntegerField()
    def __unicode__(self):
        return self.name
class testData(models.Model):
    ID = models.IntegerField(primary_key = True)
    totalA = models.IntegerField()
    totalB = models.IntegerField()
    agreeA = models.IntegerField()
    agreeB = models.IntegerField()
    disagreeA = models.IntegerField()
    disagreeB = models.IntegerField()
    def __unicode__(self):
        return self.name