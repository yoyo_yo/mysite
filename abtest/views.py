#coding=utf-8
from django.shortcuts import render,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from django import forms
import models
# Create your views here.
testNum = 1

def login(request):
    return render_to_response('login.html')

def dispatcher(request):
    s = request.GET['q']
    l = models.abtest_schema.objects.filter(name = s)
    testDb = models.testData.objects.filter(ID=testNum)
    if not testDb:
        models.testData.objects.create(ID=testNum, totalA=0, totalB=0, agreeA=0, agreeB=0, disagreeA=0, disagreeB=0)
        testDb = models.testData.objects.filter(ID=testNum)
    testDb = testDb[0]
    if l:
        pageTpye = l[0].abType
    else:
        totalA = models.testData.objects.filter(ID = 1)[0].totalA
        totalB = models.testData.objects.filter(ID = 1)[0].totalB
        if totalA*19 <= totalB:
            testDb.totalA += 1
            abType = 1
        else:
            testDb.totalB += 1
            abType = 0
        testDb.save()
        pageTpye = abType
        models.abtest_schema(name = s, abType = abType, choice = -1).save()
    if pageTpye:
        request.session['name'] = s
        return render_to_response('typeA.html')
    else:
        request.session['name'] = s
        return render_to_response('typeB.html')
def agreeA(request):
    userId = request.session['name']
    user = models.abtest_schema.objects.filter(name = userId)[0]
    testDb = models.testData.objects.filter(ID=1)[0]

    if user.choice == -1:
        user.choice = 1
        testDb.agreeA += 1
    elif user.choice == 0:
        user.choice = 1
        testDb.agreeA += 1
        testDb.disagreeA -= 1
    user.save()
    testDb.save()
    return render_to_response("exit.html")

def agreeB(request):
    userId = request.session['name']
    user = models.abtest_schema.objects.filter(name=userId)[0]
    testDb = models.testData.objects.filter(ID=1)[0]

    if user.choice == -1:
        user.choice = 1
        testDb.agreeB += 1
    elif user.choice == 0:
        user.choice = 1
        testDb.agreeB += 1
        testDb.disagreeB -= 1
    user.save()
    testDb.save()
    return render_to_response("exit.html")

def disagreeA(request):
    userId = request.session['name']
    user = models.abtest_schema.objects.filter(name=userId)[0]
    testDb = models.testData.objects.filter(ID=1)[0]

    if user.choice == -1:
        user.choice = 0
        testDb.disagreeA += 1
    elif user.choice == 1:
        user.choice = 0
        testDb.disagreeA += 1
        testDb.agreeA -= 1
    user.save()
    testDb.save()
    return render_to_response("exit.html")

def disagreeB(request):
    userId = request.session['name']
    user = models.abtest_schema.objects.filter(name=userId)[0]
    testDb = models.testData.objects.filter(ID=1)[0]

    if user.choice == -1:
        user.choice = 0
        testDb.disagreeB += 1
    elif user.choice == 1:
        user.choice = 0
        testDb.disagreeB += 1
        testDb.agreeB -= 1
    user.save()
    testDb.save()
    return render_to_response("exit.html")